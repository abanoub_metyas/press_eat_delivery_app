import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, Platform} from 'ionic-angular';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {base_page} from '../base_page'
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import {FcmProvider} from '../../providers/fcm/fcm';


@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {
    data: Observable<any>;
    public items: any = [];
    form: FormGroup;
    public device_id="123";

    validation_messages = {
        'email': [
            {type: 'email', message: 'Please enter a email like examply@gmail.come'}
        ],
        'password': [
            {type: 'password', message: 'Please enter your password .'}
        ],
    }



    constructor(
        public navCtrl: NavController,
        public formBuilder: FormBuilder,
        protected http: Http,
        private alertCtrl: AlertController,
        public modalCtrl: ModalController,
        public translate: TranslateService,
        public fcm: FcmProvider,
        private platform:Platform

    ) {

        this.form = this.formBuilder.group({

            email: ['', Validators.compose([Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
            password: ['', Validators.required]
        });

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);

        if (
            this.platform.is('android')||
            this.platform.is('ios')
        ) {

            let set_device_token=localStorage.getItem("set_device_token");

            if(set_device_token==null){
                this.fcm.getToken().then(device_token => {
                    this.device_id=device_token;
                });
            }

            this.fcm.listenToNotification().subscribe(msg => {

                if  (msg.page=="orders_page"){
                    let modal = this.modalCtrl.create('OrdersPage');
                    modal.present();
                }

                if  (msg.page=="order_page"){
                    let pass:any=[];
                    pass.push(msg.order_id);
                    let modal = this.modalCtrl.create('OrderdetailsPage', pass);
                    modal.present();
                }



            });


        }


        this.check_user_logged_in();

    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    change_lang(lang){
        localStorage.setItem("selected_lang",lang);

        this.translate.setDefaultLang(lang);
        this.translate.use(lang);
    }

    ionViewDidEnter() {

    }

    login() {

        if (this.form.valid) {

            var url = base_page.api_url() + 'api/v1/delivery/Login';

            let postData = {
                "email": this.form.get('email').value,
                "password": this.form.get('password').value,
                "device_id": this.device_id
            };


            this.data = this.http.post(url, postData);
            this.data.subscribe(data => {

                this.items=[(JSON.parse(data._body))];

                if (this.items[0].message == 1) {

                    localStorage.setItem("userid", this.items[0].user_id);
                    localStorage.setItem("token", this.items[0].token);
                    localStorage.setItem("device_id", this.items[0].device_id);
                    localStorage.setItem("address", this.items[0].address);
                    localStorage.setItem("rest_id", this.items[0].rest_id);
                    localStorage.setItem("first_name", this.items[0].first_name);


                    this.navCtrl.push('HomePage');
                }
                else{

                    let alert = this.alertCtrl.create({
                        title: this.get_trans("notification"),
                        subTitle: this.get_trans(this.items[0].message)
                    });
                    alert.present();

                }
            });

        }
        else {
            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans("all_fields_are_required")
            });
            alert.present();
        }

    }


    check_user_logged_in(){

        if (localStorage.getItem("userid") != null) {
            this.navCtrl.push('HomePage');
        }
    }


}

