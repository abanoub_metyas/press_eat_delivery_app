import {Component} from '@angular/core';
import {
    IonicPage, NavController, NavParams, PopoverController,
    ModalController, AlertController, ViewController
} from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import {TranslateService} from '@ngx-translate/core';
import {base_page} from '../base_page';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';

declare var google;

@IonicPage()
@Component({
    selector: 'page-my-orders',
    templateUrl: 'my-orders.html',
})
export class MyOrdersPage {

    location: any;
    myLat: any;
    myLong: any;
    selected_lang: any;
    rest_orders:any=[];
    data: Observable<any>;

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public popoverCtrl: PopoverController,
        public modalCtrl: ModalController,
        private geolocation: Geolocation,
        private alertCtrl: AlertController,
        public translate: TranslateService,
        protected http: Http,
        public viewCtrl: ViewController
    ) {
        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }


    dismiss() {
        this.viewCtrl.dismiss();
    }

    go_to_home() {
        this.navCtrl.push('HomePage');
    }

    ionViewDidEnter() {
        this.geolocation.getCurrentPosition().then(pos => {

            this.myLat = pos.coords.latitude;
            this.myLong = pos.coords.longitude;


            let geocoder = new google.maps.Geocoder;
            let latlng = {lat: pos.coords.latitude, lng: pos.coords.longitude};
            geocoder.geocode({'location': latlng}, (results, status) => {
                localStorage.setItem("city", results[0].address_components[0].long_name);
                this.location = localStorage.getItem('city');
            });

        });

        if (localStorage.getItem("token") == null) {
            this.navCtrl.push('LoginPage')
        }


        //get rest orders
        var url = base_page.api_url() + 'api/v1/delivery/get_delivery_man_orders';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            let ajax_data:any=[];
            ajax_data=JSON.parse(data._body);

            if (ajax_data.message == 1) {
                this.rest_orders=ajax_data.data;
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }
        });

    }

    orderdetail(id) {
        let pass:any=[];
        pass.push(id);
        let modal = this.modalCtrl.create('OrderdetailsPage', pass);
        modal.present();
    }

    sum(num1,num2){
        return parseFloat(num1)+parseFloat(num2);
    }


}
