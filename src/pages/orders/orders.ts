import {Component} from '@angular/core';
import {
    IonicPage, NavController, NavParams, PopoverController,
    ModalController, AlertController,ViewController
} from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import { TranslateService } from '@ngx-translate/core';
import {base_page} from '../base_page';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {ajax} from "rxjs-compat/umd";

declare var google;

@IonicPage()
@Component({
    selector: 'page-orders',
    templateUrl: 'orders.html',
})
export class OrdersPage {

    location: any;
    myLat: any;
    myLong: any;
    selected_lang: any;
    rest_orders:any=[];
    data: Observable<any>;
    uploads_url=base_page.api_uploads_url();
    selecetd_direction="ar";

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public popoverCtrl: PopoverController,
        public modalCtrl: ModalController,
        private geolocation: Geolocation,
        private alertCtrl: AlertController,
        public translate: TranslateService,
        protected http: Http,
        public viewCtrl: ViewController
    ) {
        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null||selected_lang==""){
            selected_lang="ar";
        }

        this.selecetd_direction=selected_lang;

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }


    dismiss() {
        this.viewCtrl.dismiss();
    }

    go_to_home() {
        this.navCtrl.push('HomePage');
    }

    ionViewDidEnter() {
        this.geolocation.getCurrentPosition().then(pos => {

            this.myLat = pos.coords.latitude;
            this.myLong = pos.coords.longitude;


            let geocoder = new google.maps.Geocoder;
            let latlng = {lat: pos.coords.latitude, lng: pos.coords.longitude};
            geocoder.geocode({'location': latlng}, (results, status) => {
                localStorage.setItem("city", results[0].address_components[0].long_name);
                this.location = localStorage.getItem('city');
            });

        });

        if (localStorage.getItem("token") == null) {
            this.navCtrl.push('LoginPage')
        }


        //get rest orders
        var url = base_page.api_url() + 'api/v1/delivery/Orders';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            let ajax_data:any=[];
            ajax_data=JSON.parse(data._body);

            if (ajax_data.message == 1) {
                this.rest_orders=ajax_data.data;
            }
            else if(ajax_data.message == "you_are_banned_so_you_can_not_show_new_orders") {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle:
                    this.get_trans(ajax_data.message) +
                    this.get_trans("ends_at") +
                    ajax_data.ends_at
                });
                alert.present();

            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }
        });

    }

    reject_accept_order(order_id,reject_or_accept){

        let url = base_page.api_url() + 'api/v1/delivery/reject_accept_order';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "order_id": order_id,
            "reject_or_accept": reject_or_accept,
        };

        let xhr_data: Observable<any>;

        xhr_data = this.http.post(url, postData);
        xhr_data.subscribe(data => {
            let ajax_data:any=[];
            ajax_data=JSON.parse(data._body);

            $(".order_block_"+order_id).hide();

            let alert = this.alertCtrl.create({
                title: this.get_trans("notification"),
                subTitle: this.get_trans(ajax_data.message)
            });
            alert.present();

            if(ajax_data.message=="order_is_1"){
                let pass:any=[];
                pass.push(order_id);
                let modal = this.modalCtrl.create('OrderdetailsPage', pass);
                modal.present();
            }
            else if(ajax_data.message == "you_are_banned_so_you_can_not_show_new_orders") {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle:
                    this.get_trans(ajax_data.message) +
                    this.get_trans("ends_at") +
                    ajax_data.ends_at
                });
                alert.present();

            }

            this.ionViewDidEnter();
        });

    }

}
