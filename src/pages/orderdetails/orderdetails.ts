import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController, ModalController} from 'ionic-angular';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {base_page} from '../base_page';
import { AlertController } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';


@IonicPage()
@Component({
    selector: 'page-orderdetails',
    templateUrl: 'orderdetails.html',
})
export class OrderdetailsPage {

    data: Observable<any>;
    id: any = [];
    items: any = [];
    item: any = [];
    public uploads_url=base_page.api_uploads_url();
    selected_driver:any="";
    allowed_sattues=[4,5];

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public viewCtrl: ViewController,
        public modalCtrl: ModalController,
        protected http: Http,
        private alertCtrl: AlertController,
        public translate: TranslateService


    ) {
        this.id = this.navParams.data;
        this.uploads_url=base_page.api_uploads_url();

        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);
    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    ionViewDidEnter() {
        var url = base_page.api_url() + 'api/v1/delivery/Order';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "id": this.id[0],
        }

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            this.items=[JSON.parse(data._body)];
            if (this.items[0].message == 1) {

                this.item=[(this.items[0].data)];
                this.selected_driver=this.item[0].order_obj.driver_id;
            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }

        });
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

    go_to_home() {
        this.navCtrl.push('HomePage');
    }

    change_order_status(status_id) {


        let alert = this.alertCtrl.create({
            title: this.get_trans("are_you_sure"),
            buttons: [
                {
                    text: this.get_trans("no"),
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.get_trans("yes"),
                    handler: () => {


                        var url = base_page.api_url() + 'api/v1/delivery/change_order_status';

                        let postData = {
                            "token": localStorage.getItem("token"),
                            "userid": localStorage.getItem("userid"),
                            "device_id": localStorage.getItem("device_id"),
                            "order_id": this.id[0],
                            "status_id": status_id,
                        };

                        this.data = this.http.post(url, postData);
                        this.data.subscribe(data => {

                            this.items=[JSON.parse(data._body)];

                            if (this.items[0].message == 0) {
                                let alert = this.alertCtrl.create({
                                    title: this.get_trans("notification"),
                                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                                });
                                alert.present();
                            }
                            else if (this.items[0].message != 0 && this.items[0].message != 1) {
                                let alert = this.alertCtrl.create({
                                    title: this.get_trans("notification"),
                                    subTitle: this.get_trans(this.items[0].message)
                                });
                                alert.present();
                            }
                            else if(this.items[0].message == 1 ){
                                this.ionViewDidEnter();
                            }


                        });

                    }
                }
            ]
        });

        alert.present();

    }

}
