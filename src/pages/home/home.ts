import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController, AlertController} from 'ionic-angular';
import {Geolocation} from '@ionic-native/geolocation';
import { TranslateService } from '@ngx-translate/core';
import {base_page} from '../base_page';
import {Http} from "@angular/http";
import {Observable} from 'rxjs/Observable';
import {OrdersPage} from "../orders/orders";
import {MyOrdersPage} from "../my-orders/my-orders";

declare var google;
declare var $:any;

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})

export class HomePage {
    location: any;
    myLat: any;
    myLong: any;
    selected_lang: any;
    data: Observable<any>;
    public first_name:any;
    public user_obj:any={};

    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        private geolocation: Geolocation,
        private alertCtrl: AlertController,
        public translate: TranslateService,
        protected http: Http
    ) {
        let selected_lang:any;
        selected_lang=localStorage.getItem("selected_lang");
        if(selected_lang==null){
            selected_lang="ar";
        }

        this.translate.setDefaultLang(selected_lang);
        this.translate.use(selected_lang);

        this.first_name=localStorage.getItem("first_name");
    }

    ionViewDidEnter() {
        this.check_user_logged_in();
        this.get_user_location();
        this.get_driver_data();
    }

    refresh(){
        this.ionViewDidEnter();
    }

    logout() {

        let alert = this.alertCtrl.create({
            title: this.get_trans("are_you_sure"),
            buttons: [
                {
                    text: this.get_trans("no"),
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: this.get_trans("yes"),
                    handler: () => {

                        let url = base_page.api_url() + 'api/v1/logout';

                        let postData = {
                            "token": localStorage.getItem("token"),
                            "user_id": localStorage.getItem("userid"),
                            "device_id": localStorage.getItem("device_id"),
                        };

                        let xhr_data: Observable<any>;

                        xhr_data = this.http.post(url, postData);
                        xhr_data.subscribe(data => {

                            localStorage.clear();
                            this.navCtrl.setRoot('LoginPage');

                        });

                    }
                }
            ]
        });

        alert.present();

    }

    get_trans(keyword){
        let trans_val:any="";

        this.translate.get(keyword).subscribe(
            value => {
                // value is our translated string
                trans_val=value;
            }
        );

        return trans_val;
    }

    change_lang(lang){
        localStorage.setItem("selected_lang",lang);

        this.translate.setDefaultLang(lang);
        this.translate.use(lang);
    }

    check_user_logged_in(){
        if (localStorage.getItem("token") == null) {
            this.navCtrl.push('LoginPage')
        }
    }

    get_user_location(){
        this.geolocation.getCurrentPosition().then(pos => {

            this.myLat = pos.coords.latitude;
            this.myLong = pos.coords.longitude;


            let geocoder = new google.maps.Geocoder;
            let latlng = {
                lat: pos.coords.latitude,
                lng: pos.coords.longitude
            };

            localStorage.setItem("lat_val", pos.coords.latitude.toString());
            localStorage.setItem("lng_val", pos.coords.longitude.toString());


            geocoder.geocode({'location': latlng}, (results, status) => {
                localStorage.setItem("city", results[0].address_components[0].long_name);
                this.location = localStorage.getItem('city');
            });

        });
    }

    get_driver_data(){

        let url = base_page.api_url() + 'api/v1/delivery/get_delivery_data';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        };

        let xhr_data: Observable<any>;

        xhr_data = this.http.post(url, postData);
        xhr_data.subscribe(data => {
            let ajax_data:any=[];
            ajax_data=JSON.parse(data._body);

            if(ajax_data.message==1){

                this.user_obj=ajax_data.user_obj;

                $(".available_btn").removeClass("active");
                $(".change_delivery_status_"+this.user_obj.freelancer_delivery_availability).addClass("active");

            }
            else{
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans(ajax_data.message)
                });
                alert.present();
            }

        });


    }

    orders_page(){
        let modal = this.modalCtrl.create('OrdersPage');
        modal.present();
    }

    current_order(){

        let url = base_page.api_url() + 'api/v1/delivery/get_current_order';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
        };

        let xhr_data: Observable<any>;

        xhr_data = this.http.post(url, postData);
        xhr_data.subscribe(data => {
            let ajax_data:any=[];
            ajax_data=JSON.parse(data._body);

            if(ajax_data.message==1){
                let pass:any=[];
                pass.push(ajax_data.order_id);
                let modal = this.modalCtrl.create('OrderdetailsPage', pass);
                modal.present();
            }
            else{
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans(ajax_data.message)
                });
                alert.present();
            }

        });


    }


    my_orders_page(){
        let modal = this.modalCtrl.create('MyOrdersPage');
        modal.present();
    }

    change_delivery_status(delivery_status){

        var url = base_page.api_url() + 'api/v1/delivery/change_delivery_status';

        let postData = {
            "token": localStorage.getItem("token"),
            "userid": localStorage.getItem("userid"),
            "device_id": localStorage.getItem("device_id"),
            "delivery_status": delivery_status
        };

        this.data = this.http.post(url, postData);
        this.data.subscribe(data => {

            let ajax_data:any=[];
            ajax_data=JSON.parse(data._body);

            if (ajax_data.message == 1) {

                $(".available_btn").removeClass("active");
                $(".change_delivery_status_"+ajax_data.delivery_status).addClass("active");

            }
            else {
                let alert = this.alertCtrl.create({
                    title: this.get_trans("notification"),
                    subTitle: this.get_trans("please_logout_then_login_because_your_session_is_expired")
                });
                alert.present();
            }
        });


    }


}
