import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import {IonicPageModule} from 'ionic-angular';
import {MyApp} from './app.component';
import {FormsModule} from '@angular/forms';
import {Geolocation} from '@ionic-native/geolocation';
import {NativeGeocoder} from '@ionic-native/native-geocoder';

import {HttpModule} from '@angular/http';
import {HttpClientModule,HttpClient} from '@angular/common/http';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {StarRatingModule} from 'ionic3-star-rating';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/langs/', '.json');
}

import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';

import {Firebase} from '@ionic-native/firebase';
import {FcmProvider} from '../providers/fcm/fcm';

const firebase_config = {
    apiKey: "AIzaSyBmlX2whNH1BK_5TIpYer388_NI3HeQJnI",
    authDomain: "press-eat.firebaseapp.com",
    databaseURL: "https://press-eat.firebaseio.com",
    projectId: "press-eat",
    storageBucket: "press-eat.appspot.com",
    messagingSenderId: "342941330103"
};


@NgModule({
    declarations: [
        MyApp
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        StarRatingModule,
        IonicPageModule.forChild(MyApp),
        HttpClientModule,
        IonicModule.forRoot(MyApp, {
            tabsPlacement: 'top',
            platforms: {
                android: {
                    tabsPlacement: 'top'
                },
                ios: {
                    tabsPlacement: 'top'
                },
                windows: {
                    tabsPlacement: 'top'
                }
            }
        }),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AngularFireModule.initializeApp(firebase_config),
        AngularFirestoreModule
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp
    ],
    providers: [
        StatusBar,
        Geolocation,
        NativeGeocoder,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        FcmProvider,
        Firebase
    ]
})
export class AppModule {
}
