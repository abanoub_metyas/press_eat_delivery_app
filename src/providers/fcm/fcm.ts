import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Firebase} from '@ionic-native/firebase';
import {Platform} from 'ionic-angular';
import {AngularFirestore} from "@angular/fire/firestore";

@Injectable()
export class FcmProvider {


    constructor(
        public http: HttpClient,
        public firebaseNative:Firebase,
        public afs:AngularFirestore,
        private platform:Platform
    ) {

    }

    async getToken(){

        let Token;

        if (
            this.platform.is('android')||
            this.platform.is('ios')
        ) {
            Token=await this.firebaseNative.getToken();
            if (this.platform.is('ios')){
                await this.firebaseNative.grantPermission();

            }

            return Token;
        }
    }

    listenToNotification(){

        return this.firebaseNative.onNotificationOpen();

    }


}
